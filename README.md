This is a repository containing all my walkthroughs. I no longer contribute to GFAQs since I created [my own website](https://barrelwisdom.com/hub/Main_Page), so this is more of an archive/backup than anything.

The series guides were made for Reddit, so you'll see some oddness in them due to slight markdown differences.
