>Okay, you know how every RPG has the grand heroes chosen by fate to become wonderfully friends and stand up to the evil sorcerer/empire/cooperation/monster and keep it from destroying the world? This game is not about them.

>You know how in almost every RPG, buried about halfway down the final Dungeon, sitting next to the second to last save point in the game there's a shopkeeper who has every healing item in the game and every bit of weapons and armor that don't require an epic quest? The one who leaves you wondering "Just how in blazes did he get down here? I'm the ridiculously powerful chosen one and it's almost impossible for me to do it so how did a lowly shopkeeper pull it off"?

>This game is about her.

--[JarakinPOL](http://www.gamefaqs.com/boards/953050-atelier-annie-alchemists-of-sera-island/52080680), summarizing the spirit of the series.


Atelier is basically the "original" item making RPG series. Everything else is a shameful imposter. Probably. More seriously, the first entry in the series, Atelier Marie, is thought to have popularized the concept of item crafting in later RPGs. Atelier remains the king, queen, and especially princess of item creation, and all others bow before it nearly 20 years later.

The series itself is thus known for incredibly deep item crafting systems, and the games -- from exploration to monster drops -- revolve around this aspect. They otherwise feature standard turn based battle systems. The game progression varies significantly; many have time limits and involve mild resource management, and the rest don't have time limits at all but still vary a lot. The more recent games are further known for cute girls doing cute things. You will have opinions on Cutest Alchemy Girl and if you say it isn't Totori you are factually wrong.

Here are all of the Atelier games that are or will be in English:

* **Iris Trilogy** 1-3 ^((PS2)^)
* **Mana Khemia** 1 ^((PS2|PSP)^) and 2 ^((PS2)^)
* **Arland Trilogy**: Rorona ^((PS3|Vita|PS Now)^), Totori and Meruru ^((PS3|Vita)^)
* **Dusk Trilogy**: Ayesha, Escha & Logy, and Shallie ^((PS3|Vita|PS Now)^)
* **Mysterious**: Sophie and Firis ^((Vita|PS4|PC)^)
* **Spinoff**: Annie ^((DS)^)
* **Salburg**: Marie+Elie ^((PS2)^) Fan Translation (In Progress)

The only games that are related are those within the same subseries. So Mysterious has no ties to Dusk, Dusk has no ties to Iris, and so on. You will see characters like Pamela, Logy, Escha, and Hagel appear in several subseries, but these are different characters between each subseries.

There is something of a divide in the series. Iris and Mana Khemia are unlike the "traditional" games that comprise the rest of the series. They are, simply put, more traditional RPGs, especially the Iris trilogy.

# Where Should I Start?

First, here are the two worst options: Meruru and Shallie. Being conclusions, they benefit a lot from knowing who all the returning characters are.

If you are extremely allergic to a time limit, Escha & Logy exists as a "hidden" option in that the time limit is so easy they shouldn't have bothered. It's just not the absolute best starting place otherwise.

### Ayesha

* If there isn't a killer sale on something else, **START HERE** and don't think about the rest.
* The time limit isn't challenging.
* It teaches you everything Atelier is about in a very palatable package.
* I'd say it has the most appeal of the series due to its really nice story, and the gameplay is still representative of what you'll find in the series.
* Note: PS3 version is available on PS Now (and thus, PC and PS4). PS3 versions of Escha & Logy and Shallie are also available on this service.

### Sophie

* This would be the "new and shiny" option. There aren't any strings attached; if you're interested in it, play it.
* The main feature of this game is that it lacks a time limit and has an extremely unique synthesis system.
* A lot of veterans would not recommend this game as a starter. However, I've seen a large number of noobs play this as their first title and enjoy it. If you're stuck to PC or otherwise determined to play the latest, you really don't have anything to fear starting here, whatever we grumpy veterans tell you about it.
* The PC port ain't perfect but it sounds decent. I hesitate to say too much as of this writing; it's better to do your own research so you can have the most recent info. I feel like patches are coming for the remaining problems.

### The Arland Conundrum: Rorona Plus or Totori?

* There's no correct answer.
* "I Don't Want To Think" Answer: Rorona. While Totori was recommended historically, Rorona is just more noob friendly overall.
* Totori
 * The older game that gives you little guidance.
 * Lots of freedom.
 * Harder for new people.
 * First in release order.
 * Ridiculous cute.
* Rorona Plus
 * Holds your hand better.
 * There is a notable downgrade moving to Totori.
 * Has a fetch questy mindset.
 * In some regards, the hardest game in the series. Mainly if you want to do everything possible.
 * First in chronological order.
 * Extremely charming.
* My Advice
 * If you play Rorona: Play it very casually and avoid the fetch quest trap. Prepare for NG+ if you're interested in doing everything.
 * If you play Totori: Learn to make good enough equipment so you can beat That One Boss. Secondly, when you can make the boat... SET SAIL, sooner rather than later, and head east to get the normal end. Keep these points in mind and the game is easy.
 * For Arland as a whole: You will see items that improve your walking speed, gather speed, etc. You will make those ASAP. They do help.
* Note: Rorona Plus is available on PS Now (and thus, PC and PS4). However, Totori and Meruru have not yet been added.

# The Finer Points

This series is really expansive. I try to make succinct summaries of what each subseries is about, but there sure are a lot of words here and I wouldn't blame you for skipping some of them.

## Non-Traditional

### Iris
* Outright traditional RPGs with simple crafting systems. You've basically already imagined what the first two games are like.
* 1&2 are related.
* 3 isn't related to the others at all. But it's also very experimental in structure, as you have a time limit when you visit dungeons. (You can revisit as much as you want, just to be clear.)
* This trilogy doesn't really represent what makes Atelier special, but they can be fun games.
* Some people think it's a shame I brush this trilogy off, so I'll have a sort of long-winded discussion about this to fill the void.
 * This series is the least 'Atelier' of them all. That's the short of it.
 * The fact that Iris and MK were localized first set up the wrong expectations in the west, which I believes gives some people the wrong idea of how important these games are.
 * The modern games appear to have gone totally nuts in comparison to these 'first' games. It's a wonder how this modern moe explosion and these older, fairly normal JRPGs are in the same series, right?
 * Some people seem to be under the misconception that Iris is the first in the series. Iris 1 is actually the sixth. So in other words: these crazy modern games make a lot more sense when you look at the first through fifth games, Marie through Viorate.
 * From a historical standpoint: Rorona looked back at the classics and set the stage for modern games. Iris, in comparison to other titles, just doesn't have much of a place in the series.
 * So should you play these games? I don't see why not. But I won't recommend them as *Atelier* games so much as plain-o JRPGs.

### Mana Khemia
* Yes, these are Atelier. Yes, they have funny names. No, they aren't spinoffs.
* Would appeal to anyone wanting a traditional RPG, but it isn't as "JRPG" as the Iris trilogy.
* Game Progression
 * This is basically Atelier-goes-to-school.
 * You're given assignments to do as part of your classes. These involve all the traditional RPG things (exploration, fighting, a bit of plot), plus item creation.
* Gameplay
 * The best battle systems in the series. The later two Dusk games directly ripped off MK2, in fact.
 * They feature an interesting skill/stat progression method -- the grow book. As you make items, you unlock skills and such.
 * Synthesis has some interesting points to it. Still simpler than the modern games.
* Story
 * Simple stories.
 * MK2 features dual protagonists, and each has a substantially different set of events.
 * Character focused.


## Traditional

### Arland

* Story order: Rorona -> Totori -> Meruru
* Release order: Totori -> Meruru -> Rorona PLUS
* The quintessential traditional Atelier subseries.
 * These are the "scary" time management games you heard of. They aren't hard, but you can't approach them mindlessly.
 * These have a hard focus on item creation, and explore synthesis in increasing amounts of depth.
 * Unlike most RPGs, you can't grind or item farm. Keep that in mind and you will do just fine.
*  Game Progression
 * Rorona gives you an assignment to do every 3 months.
 * Totori gives you almost total freedom, but expects you to "rank up" as an adventurer doing a variety of small tasks, which range from making items to defeating bosses.
 * Meruru expects you to, essentially, improve the kingdom and boost its population. You'll do a lot of smaller tasks, which award points that you will spend to make the kingdom better.
* Story
 * Very slice of life stories.
 * Goofy casts you'll never forget.
* Plus Versions
 * PS3 DLC is tossed in for free.
 * **Only consider the PLUS version of Rorona,** which is available for both PS3 and Vita. It's a partial and much needed remake of the original Rorona.
 * Totori+ includes minor synthesis tweaks and a bonus dungeon. Easily the weakest of the lot.
 * Meruru+ adds an ending. The ending the original needed most desperately. And looooots of minor adjustments.
 * Avoid the PS3 collection of the Arland games; it includes the original Rorona.
 * The Vita collection is totally fine.
 * Rorona Plus has a bonus overtime scenario that includes Totori and Meruru. It's probably better as a post-Meruru challenge than something you feel obligated to do.

### Dusk

* Order: Ayesha -> Escha & Logy -> Shallie
* Ayesha has a relaxed time limit. E&L has a joke of a time limit. And Shallie removes it.
* Story
 * More serious in tone than Arland. Still primarily slice of life, but has plot.
 * Ayesha has a really good setting and story.
 * The sequels don't properly follow up on this.
 * E&L and Shallie feature dual protagonists. Most of the game is the same between the two routes, though you do get some exclusive events, items, and endings.
* Gameplay
 * The two later games have the most polished gameplay in the series.
 * They rip off MK2's battle system, which is a good thing.
 * The synthesis is quite deep and satisfying.
 * Ayesha's good enough in this respect. It's predecessor, Meruru, and its sequels are just better.
* Game Progression
 * Ayesha expects you to try to save Nio. You're mostly given freedom but you will find plot events every now and then.
 * E&L give you an assignment every four months, as well as a variety of smaller tasks. You have tons of free time to do whatever.
 * Shallie is divided into plot events and free time. To advance out of free time you have to do some small tasks.
* Plus Versions
 * PS3 version DLC is included.
 * Ayesha has hard mode and includes the Japanese voices, which is probably huge to some of you. The PS3 version is the one title that doesn't have the Japanese track at all.
 * E&L has a few bosses and a new playable character. There are also new events between the two leads which you can choose to make more romantic or platonic.
 * Shallie has substantial additions, as it includes Ayesha and Logy. [I made a playlist of all the new stuff.](https://www.youtube.com/playlist?list=PLQktyFBCcQggmp87_SyHJ-et1SsOv3bzy) I won't call it a perfect solution, but for those of you who are PS3 bound, this is my attempt to make it better.


### Mysterious

* Order: Sophie -> Firis
* Time
 * Firis has a partial and extremely easy time limit. Gust is confused.
 * Those of you terrified of the words "time limit" should be okay with this. It's laughably easy, and you can do anything you want afterward.
 * Sophie has a time of day system, which affects events and a number of gameplay particulars. No time limit.
* Game Progression
 * You make items.  You do things to unlock item recipes. The things you do don't necessarily make sense.
 * No, really. There's a tiny bit of plot in there but that's the gist of how both the Mysterious games work, and how you progress Sophie's plot.
 * Firis just mandates that you travel to a distant city and pass an alchemy exam within a time limit. It's fairly nonlinear and free in how you choose to do this.
* Gameplay 
 * The primary point of interest is its synthesis system. It isn't simply "pick items in a list", like previous games. Rather, the size of items and positions are more important. Kind of like a puzzle.
 * [Here's a video of Sophie](https://www.youtube.com/watch?v=hpZISjUSNJs), because I think words don't help. Heck, this is really a system you learn better by doing. Firis has the same fundamental idea but makes some improvements.
 * While Sophie's battle system isn't anything special, Firis expands and improves upon the ideas significantly.
* Story
 * Plot largely goes back to Arland levels of extreme slice of life. Sophie entertains a tiny bit of story, but Firis really just sticks to character events.


### Other Games

* I'm not gonna recommend Annie to you.
 * It's a spinoff, and its quality reflects that.
 * Hold-y-to-win battle system, uninteresting sim aspects, fetch questy, basic synthesis system.
 * You could do worse, but maybe look at Gust's other offerings before looking here.
 * They waste a sexy voice like Miyuki Sawashiro on a fairy named Pepe.

* On Marie+Elie, it's been Japanese only till this point and as such I can't tell you much about it except that I am going to play the crap out of it once the fan translation is done. I expect simplicity, at least.


# FAQ

* Are the Mysterious games getting Plus versions?
 * I hope not! Plus generally refers to a Vita port with minor bonuses. These games were released for Vita simultaneously, so I can't see a reason for it. But then again, they've been Plus-ing since the very first game in the series. They say "no", but I won't believe it until several years have passed.

* How do the Vita versions perform?
 * Frame rate pretty much always takes a hit, though it varies by game. I found Totori and E&L to be tolerable, for instance, but I have heard Bad Things about Sophie and Ayesha. Shallie is sometimes very, very ugly and it hurts my eyes to look at some locations.

* Are the Vita versions all digital?
 * Mostly. Escha & Logy and Shallie had limited editions, and Shallie had a regular physical edition in EU.

* How hard is the time limit?
 * The only game this is particularly a problem in is Totori. It's generally quite easy for other games.
 * From hardest to easiest: Rorona+ (Total Completion) > Totori > Meruru > Ayesha > Rorona+ (Casual) > Escha & Logy
 
* What's this about barrels?
 * I don't know either. In the very first game, they had Marie say it when she interacted with a barrel and it's been a thing ever since. In recent times, it has elevated to trophy status.

* How do you pronounce Atelier?
 * [Listen to these two ways.](http://www.oxfordlearnersdictionaries.com/us/definition/english/atelier) It claims one is British and the other is North American. I claim it's such an unusual French word it doesn't matter. Pick the one you like. Pick the original French way if bastardizations aren't your thing. And crush all over ways of saying it.
 * Don't make a filthy R sound when you say the name of the series *you heathens.*
 * You're not allowed to be friends with anyone that calls it at-lee-er, [as the dubs do](https://youtu.be/e-fqRbm4CVk?t=13m41s). That's so wrong it's criminal.