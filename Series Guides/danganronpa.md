First, let it be said. It's probably too late already, but I will try to warn all who read this:

* Do not look up this series.
* Do not go to the wikia.
* Do not browse this subreddit any further.
* Do not look at promotional images. (Really!)

There are spoilers everywhere. I know it's hard to avoid knowing who survives DR1 at this point, but stay away from all things Danganronpa.


# The Essential Order -- Hope's Peak Academy

I'm going to be honest here: You can stick to DR1 and DR2 and have a perfectly fine experience to walk away from. It's okay to not want to devote more of your life to this ever growing multimedia franchise. If you **do** want to enjoy the series as a whole, however, here is what you need to know.

1. **Danganronpa 1** (Game) *or* **Danganronpa: The Animation**
 * Systems: [Vita](https://www.playstation.com/en-us/games/danganronpa-trigger-happy-havoc-psvita/), [PS4 (1+2 Reload)](https://www.playstation.com/en-us/games/danganronpa-1-2-reload-ps4/), [PC/Mac/Linux](http://store.steampowered.com/app/413410), [PSP (Fan Translation)](https://danganronpa.wordpress.com/)
 * Anime Links: [Funimation](https://www.funimation.com/shows/danganronpa-the-animation/), [Yahoo View](https://view.yahoo.com/show/danganronpa-the-animation)
 * Game is strongly recommended over anime.

2. **Danganronpa Zero** (Light Novel) -- *Optional/Recommended*
 * This is a prequel to the series. Provides background information.
 * There's nothing forcing you to read this before DR2, honestly. But if you're going to read it at all, make it either before or soon after DR2.
 * Translations: [Part 1](http://steampunktietime.tumblr.com/post/106314958860/dangan-ronpa-zero-translation-masterlist), [Part 2](http://revolutionpotato.tumblr.com/post/75118713692/dangan-ronpa-zero-book-2-translations). Yes, these two combined form one complete DR0 translation. I'm not aware if anyone completed the entire thing on their own.

3. **Danganronpa 2**  (Game) 
 * Systems: [Vita](https://www.playstation.com/en-us/games/danganronpa2-goodbye-despair-psvita/),  [PS4 (1+2 Reload)](https://www.playstation.com/en-us/games/danganronpa-1-2-reload-ps4/), [PC/Mac/Linux](http://store.steampowered.com/app/413420/)
 * No animated adaptation of this exists. If you lack the means to play the game, scroll down to the LP section.

4. **Danganronpa Another Episode** (Game) -- *Recommended*
 * System: [Vita](https://www.playstation.com/en-us/games/danganronpa-another-episode-ultra-despair-girls-psvita/), [PS4](https://www.playstation.com/en-us/games/danganronpa-another-episode-ultra-despair-girls-ps4/), [PC](http://store.steampowered.com/app/555950/)
 * DR3 rolls with the assumption that you know what happened here. It is not as vital to the plot as DR1 or DR2, so it's "recommended" and not "required". You can skip this if you really want to.
 * The LP section has multiple viewing options for this game. If you really, really, REALLY don't care to experience this game, [have a super short summary](http://ykb9988.tumblr.com/post/98614130121/dangan-ronpa-another-episode-mini-summary) or view this [30 minute recap](https://www.youtube.com/watch?v=PSeAKckdOm4) by Oni Black Mage.

5. **Danganronpa 3** (Anime)
 * View this anime in release order. Do not be cute and view it in whatever order you please, and don't view it in the order Funimation gives you. Funimation screwed up majorly, so I will give you the correct order below.
 * There's a 3 in the title for a reason. I realize some of you started out by watching the adaptation of the first game, but **YOU CANNOT SKIP 2**.

\# | Episode | # | Episode | # | Episode
:--:|:--|:--:|:--|:--:|:--
1 | [Future 1](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/future-arc-1-third-times-the-charm) | 9 | [Future 5](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/future-arc-5-dreams-of-distant-days) | 17 | [Future 9](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/future-arc-9-you-are-my-reason-to-die)
2 | [Despair 1](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/despair-arc-1-hello-again-hopes-peak-high-school) | 10 | [Despair 5](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/despair-arc-5-beginning-of-the-end) | 18 | [Despair 9](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/despair-arc-9-chisa-yukizome-doesnt-smile)
3 | [Future 2](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/future-arc-2-hang-the-witch) | 11 | [Future 6](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/future-arc-6-no-man-is-an-island) | 19 | [Future 10](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/future-arc-10-death-destruction-despair)
4 | [Despair 2](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/despair-arc-2-my-impurest-heart-for-you) | 12 | [Despair 6](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/despair-arc-6-a-despairfully-fateful-encounter) | 20 | [Despair 10](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/despair-arc-10-smile-at-despair-in-the-name-of-hope)
5 | [Future 3](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/future-arc-3-cruel-violence-and-hollow-words) | 13 | [Future 7](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/future-arc-7-ultra-despair-girls) | 21 | [Future 11](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/future-arc-11-all-good-things)
6 | [Despair 3](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/despair-arc-3-a-farewell-to-all-futures) | 14 | [Despair 7](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/despair-arc-7-the-biggest-most-atrocious-incident-in-hopes-peak-high-schools-history) | 22 | [Despair 11](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/despair-arc-11-goodbye-hopes-peak-high-school)
7 | [Future 4](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/future-arc-4-who-is-a-liar) | 15 | [Future 8](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/future-arc-8-who-killed-cock-robin) | 23 | [Future 12](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/future-arc-12-it-is-always-darkest)
8 | [Despair 4](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/despair-arc-4-the-melancholy-surprise-and-disappearance-of-nagito-komaeda) | 16 | [Despair 8](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/despair-arc-8-the-worst-reunion-by-chance) | 24 | [Hope](http://www.funimation.com/shows/danganronpa-3-the-end-of-hopes-peak-high-school/hope-arc-the-school-of-hope-and-the-students-of-despair)

# Ultimate Academy for Gifted Juveniles

1. **Danganronpa V3** (Game)
 * System: [Vita, PS4](http://store.nisamerica.com/preorders/danganronpa?___SID=U) (Preorder 9/26/2017)
 * This is absolutely not to be confused with DR3, the anime, which concludes the Hope's Peak storyline.
 * You need to experience DR1 through DR3 before playing this game. I'm aware you probably heard otherwise. The game's marketing is misleading.

## Side Materials

These are all entirely optional, and a lot of them are in progress. I am marking some of the titles as spoilers as a courtesy. **Note:** Spoiler boxes don't display at all on mobile, so this table will appear confusing. Switch to the desktop version if you need to see this properly.

There are actually more manga adaptations, comic anthologies, 4komas, etc.  It's kind of ridiculous to list them all, so I make no attempt to do so. 

Title | Type | Translation | Notes
:--:|:--|:--|:--
Makoto Naegi's Worst Day Ever | Short Story | [Complete](https://danganronpa.wordpress.com/2013/12/25/release-makoto-naegis-worst-day-ever/) | Read after DR1.
Danganronpa If | Short Story | -- | Unlocked after beating DR2.
Ultra Despair [](/s "Hagakure") | Short Story | -- | Unlocked after beating Another Episode.
Danganronpa [](/s "Kirigiri") |  Light Novel | [Ongoing](http://duelnoir.dreamwidth.org/4362.html) / [Partial Summaries](http://jinjojess.tumblr.com/DR-side-novel-summaries)| Can read after DR1
Danganronpa [](/s "Togami")  | Light Novel | [Volume 1 complete](http://duelnoir.dreamwidth.org/4362.html) / [Summary](http://jinjojess.tumblr.com/DR-side-novel-summaries) | Can read after DR2
Killer Killer | Manga | [Complete](http://shslscans.tumblr.com/tagged/KKManga) | Read after DR3.
Danganropna 2.5 | Anime / OVA | Yeah we don't link to those things here. | Watch after DR3.

# LP Links

Since I'm sure a lot of you want these for one reason or another. This is not a comprehensive list, but it should cover a variety of types.

* [DRAE Condensed](https://www.youtube.com/watch?v=if6Kp7jl1IM). Yes, really, this 12 hour monstrosity is the condensed story only version.

* **Hikkie**, No Commentary/JP Audio. ([DR1](https://www.youtube.com/playlist?list=PLiRI4Bw-r0a2xhUaDFyvtJluYghvUjeHW) (PSP Version) / [DR2](https://www.youtube.com/playlist?list=PLiRI4Bw-r0a3FXlqbmIzXvuDkamOrMq1z) / [DRAE](https://www.youtube.com/playlist?list=PLiRI4Bw-r0a1l6InyQDlHTkWR9RKkMyK2))

* **justonegamr**, EN Audio/No Commentary. ([DR2](https://www.youtube.com/playlist?list=PLn3Wm-3X3v7Cr-KSO0D4-svJ9gvWftlyX) / [DRAE](https://www.youtube.com/playlist?list=PLn3Wm-3X3v7C9vE_wcW71DSEfXqifi5R8))

* **Materwelonz**, JP Audio/Commentary. ([DR1](https://www.youtube.com/playlist?list=PLAIcZs9N4170wmLjrwB8URPpF0Bi8cgBY) / [DR2](https://www.youtube.com/playlist?list=PLAIcZs9N4171V2WdwLROaVdMJieN2EfgP))

* **NicoB**, EN Audio/Commentary.  ([DR1](https://www.youtube.com/playlist?list=PL5bkYBlFL9xd3xEzhbmWzj9E8_kDKrTfr) / [DR2](https://www.youtube.com/playlist?list=PL5bkYBlFL9xch7lKFo64ITGg5AyO297Ay) / [DRAE](https://www.youtube.com/playlist?list=PL5bkYBlFL9xdWA-cKpLOqmHNYyBb1ZWya))

* **Sirlionhart**, EN Audio/Commentary. ([DR1](https://www.youtube.com/playlist?list=PLOdDlDt7wGxcBxO1lgyTTiWCmF2UvE5AU) / [DR2](https://www.youtube.com/playlist?list=PLOdDlDt7wGxcBxO1lgyTTiWCmF2UvE5AU) / [DRAE](https://www.youtube.com/playlist?list=PLOdDlDt7wGxdIwU_osWfdb3y5O3CefHTb))

# Frequently Asked Questions

* **What's the difference between Danganronpa V3 and Danganronpa 3?**
  * DR3, the anime, is the conclusion to the Hope's Peak (DR1+DR2) storyline. DRV3 is an upcoming main game that's more distantly related to those games.

* **I noticed Despair Arc of the DR3 anime is a prequel. Is it okay to watch before DR2?**
  * **NO.**

* **I noticed Another Episode happens before DR2. Is it okay to play before DR2?**
 * **NO.**

* **Can I start with Danganronpa V3?**
 * No. Complete DR1 through DR3 before starting V3.

* **Is it okay to see Despair Arc before Future?**
  * No. Stick to the viewing order outlined in the first section of this guide. There is a reason, and you'll understand once you start watching. But here's one clue: the beginning of Despair 1 casually spoils the ending of Future 1.

* **I watched Danganronpa: The Animation. Do I have to play DR1?**
  * Nope. The anime's problem is basically that it condenses ~30 hours of content to just over 5 hours of animation. The major points are covered well enough, just without the detail (especially during investigations/trials, which the anime outright butchers) that the game gives you.